<?php

// ruleid: checklist-retrieve-post-parameters
echo 'Hello ' . htmlspecialchars($_POST["name"]) . '!';

// ruleid: checklist-retrieve-post-parameters
echo 'Hello ' . htmlspecialchars($_POST[$MYVAR]) . '!';

// TODO: Only worry about accessing parameters for now, so
// there's always a value extracted to autofix.
// ok: checklist-retrieve-post-parameters
$my_post = $_POST;


// ok: checklist-retrieve-post-parameters
# Preferred way to get POST parameter values
if ( $wgRequest->getVal('action') == 'purge' ) {
    echo 1;
}

// ok: checklist-retrieve-post-parameters
# Preferred way to get POST parameter values
$action = $wgRequest->getVal('action');

?>

