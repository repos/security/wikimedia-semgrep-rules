<?php

// Native PHP way to set cookie value
// ruleid: checklist-cookie-set
setcookie("TestCookie", $value);

// Preferred way to set cookie value
// ok: checklist-cookie-set
$wgRequest->response()->setCookie('value');
?>