<?php

// ruleid: checklist-retrieve-get-parameters
echo 'Hello ' . htmlspecialchars($_GET["name"]) . '!';

// ruleid: checklist-retrieve-get-parameters
echo 'Hello ' . htmlspecialchars($_GET[$MYVAR]) . '!';

// TODO: Only worry about accessing parameters for now, so
// there's always a value extracted to autofix.
// ok: checklist-retrieve-get-parameters
$MY_GET = $_GET;

// ok: checklist-retrieve-get-parameters
if ( $wgRequest->getVal('action') == 'purge' ) {
    echo 1;
}

// ok: checklist-retrieve-get-parameters
$action = $wgRequest->getVal('action');

?>

