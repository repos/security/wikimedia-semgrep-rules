<?php

// Do not use $_COOKIE to get cookie values
// ruleid: checklist-cookie-fetch
echo 'Hello ' . htmlspecialchars($_COOKIE["name"]) . '!';

// ok: checklist-cookie-fetch
// Preferred way to get cookie value
$wgRequest->getCookie('value');

# Ex: Attempt to fetch the UserID cookie value. Note: The
# value returned isn't trusted and is forced to be an int.
// ok: checklist-cookie-fetch
$sId = intval( $wgRequest->getCookie( 'UserID' ) );
?>
