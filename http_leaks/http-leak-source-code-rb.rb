# ruleid:http-leak-source-code-rb
a = "http://example.com"

# ok:http-leak-source-code-rb
# c = "http://example.com"

# ruleid:http-leak-source-code-rb
b = "https://example.com"

# ok:http-leak-source-code-rb
# d = "https://example.com"