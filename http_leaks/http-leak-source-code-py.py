# ruleid:http-leak-source-code-py
a = "http://example.com"

# ok:http-leak-source-code-py
# c = "http://example.com"

# ruleid:http-leak-source-code-py
b = "https://example.com"

# ok:http-leak-source-code-py
# d = "https://example.com"