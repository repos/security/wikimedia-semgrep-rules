<?php
// ruleid:http-leak-source-code-php
$a = "http://example.com";

// ok:http-leak-source-code-php
// c = "http://example.com"

/*
  ok:http-leak-source-code-php
  e = "http://example.com"
*/

// ruleid:http-leak-source-code-php
$b = "https://example.com";

// ok:http-leak-source-code-php
// d = "https://example.com"
?>