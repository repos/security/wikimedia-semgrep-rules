public class HttpLeakSourceCode {
    // ruleid: http-leak-source-code-java
    public String attr = "http://example.com";

    public void func(){
        // ruleid: http-leak-source-code-java
        System.out.println("https://example.com")

        // https://example.com"

        /*
            http://example.com"
        */
    }
}