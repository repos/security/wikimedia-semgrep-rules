// ruleid:http-leak-source-code-js
var a = "http://example.com"

// ok:http-leak-source-code-js
// c = "http://example.com"

// ruleid:http-leak-source-code-js
var b = "https://example.com"

// ok:http-leak-source-code-js
// d = "https://example.com"

/*
    ok:http-leak-source-code-js
    e = "https://example.com"
*/