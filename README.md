# Wikimedia Semgrep Rules

A repository of semgrep rules, many specific to Wikimedia projects and codebases.  Note that all rules within this repository MUST be licensed with an OSI or free culture compliant license.

# Usage

Rule generation for a client engagement

Run `semgrep --test` in the root of this directory to run the test cases - the
rule in `foo-bar.yml` will be evaluated against `foo-bar.<py|rb|...>` based on
the language specified in `foo-bar.yml`.

## Example: Extracting all HTTP URLs

First, we run all of the relevant results on our target and store the results as JSON.

We could pipe directly to [jq](https://github.com/stedolan/jq), but if the scan takes a few seconds to run, it's easier to iterate on slicing/dicing the data if we can just operate on the resulting JSON.

Ran from the root of this repo:

```bash
$ semgrep --config generic/ path/to/mediawiki --json > mediawiki_generic.json
```

See the raw results:

```bash
$ cat mediawiki_generic.json | jq
```

View all the matched lines (including the URLs matched):

```bash
$ cat mediawiki_generic.json| jq ' .. | .lines?' | grep -v null
# | wc -l => 525

```bash
cat mediawiki_generic.json | jq ' .. | .path?' | grep -v null | sort | uniq
```

View all the matched lines (including the URLs matched):

```bash 
$ cat mediawiki_generic.json| jq ' .. | .lines?' | grep -v null
# | wc -l => 525
```

Show only the unique matched URLs:

```bash
$ cat mediawiki_generic.json| jq ' .. | .lines?' | grep -v null | sort | uniq
```

Filter out URLs you don't care about via the CLI:

```bash
$ cat mediawiki_generic.json| jq ' .. | .lines?' | grep -v null | grep -v "https://www.mediawiki.org" | wc -l
# 362
```

Rather than one-off CLI pipes, you could also update the rule itself:
* For example, including or excluding specific paths [docs](https://semgrep.dev/docs/writing-rules/rule-syntax/#limiting-a-rule-to-paths)
* Or by adding `pattern-not` clauses that filter out specific URLs, like:

```yaml
patterns:
  pattern: |
    "=~/https?:///"
  pattern-not: |
    "=~/https?://www.mediawiki.org/"
  pattern-not: |
    "=~/https?://example.com/"
```

## Contributing

Merge requests and stars are always welcome!

## Authors

* **Scott Bassett** [sbassett@wikimedia.org]
* **Gavin Stroy**
* **Clint Gibler**

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.
