let dynamic = "function dynamicStrings() { return 'dynamic strings are not'; }"

// ruleid:dangerous-methods-1
alert(dynamic + 'possibly malicious code');

// ruleid:dangerous-methods-1
prompt(`${dynamic} possibly malicious code`);

// ruleid:dangerous-methods-1
setTimeout("eval code here",timer);

// ruleid:dangerous-methods-1
var i = setInterval(function(){}, 0);
for(; i >= 0; i-=1) {
    clearInterval(i);
}

// ruleid:dangerous-methods-1
setImmediate(help.bind(null, i+1, cb));

// ruleid:dangerous-methods-1
crypto.generateCRMFRequest('CN=0',0,0,null,'alert(1)',384,null,'rsa-dual-use')
