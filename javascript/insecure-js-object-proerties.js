

// ok: insecure-js-object-proerties
el.innerHTML = "<img src='img.jpg'>";

// ruleid: insecure-js-object-proerties
el.innerHTML = user_var;

// ok: insecure-js-object-proerties
d.outerHTML = "<p>This is a paragraph.</p>";

// ruleid: insecure-js-object-proerties
d.outerHTML = user_var;

// ruleid: insecure-js-object-proerties
(new Image()).src = "https://localhost/log_xss?from=" + window.location;

// ruleid: insecure-js-object-proerties
element.textContent = untrustedData;

// ruleid: insecure-js-object-proerties
document.getElementById('var1').innerText = 
                document.getElementById("user_var1").value;