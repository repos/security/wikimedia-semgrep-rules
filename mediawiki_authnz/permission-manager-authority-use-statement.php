<?php

// ruleid:permission-manager-authority-use-statement
use MediaWiki\Permissions\PermissionManager;

// ok:permission-manager-authority-use-statement
use MediaWiki\Foo\Example;

?>
